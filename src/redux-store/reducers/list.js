export default (state = [], action) => {
  switch (action.type) {
    case "ADD_TO_LIST":{
        const { name, data } = action.payload;
        return [
            ...state,
            {
                id: name,
                data: data
            }
        ];
    }
    case "UPDATE_IN_LIST":{
        let { name, data } = action.payload;
        let valueToUpdate =
            state &&
            state.find(value => {
                return value.id === name;
            });
        let indexValue = state.indexOf(valueToUpdate);
        return [
            ...state.slice(0, indexValue),
            {
                ...valueToUpdate,
                data: data
            },
            ...state.slice(indexValue + 1, state.length)
        ];
    }
    case "DELETE_FROM_LIST":{
        let { name } = action.payload;
        let valueToDelete =
            state &&
            state.find(value => {
                return value.id === name;
            });
        let indexValue = state.indexOf(valueToDelete);
        return [
            ...state.slice(0, indexValue),
            ...state.slice(indexValue + 1, state.length)
        ];
    }
    default:
      return state;
  }
};
