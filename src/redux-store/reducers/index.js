import { combineReducers } from "redux";
import form from "./form";
import list from "./list";
const rootReducers = combineReducers({ form, list });

export default rootReducers;
