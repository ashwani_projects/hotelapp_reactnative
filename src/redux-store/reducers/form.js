export default (state = {}, action) => {
  switch (action.type) {
    case "FORM_INIT":
      let { name } = action.payload;
      return {
        ...state,
        [name]: {}
      };
    case "ADD_DATA": {
      let { name, field, value } = action.payload;
      let formState = state[name] || {};
      formState["dataSet"] = {
        ...formState.dataSet,
        [field]: value
      };
      return {
        ...state,
        [name]: {
          ...formState
        }
      };
    }

    default:
      return state;
  }
};
