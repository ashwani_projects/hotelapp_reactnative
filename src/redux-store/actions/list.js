import Types from "./types";

var addGuest = payload => {
  return {
    type: Types.list.ADD_TO_LIST,
    payload
  };
};
var updateGuest = payload => {
  return {
    type: Types.list.UPDATE_IN_LIST,
    payload
  };
};
var deleteGuest = payload => {
  return {
    type: Types.list.DELETE_FROM_LIST,
    payload
  };
};

module.exports = {
  addGuest,
  updateGuest,
    deleteGuest
};
