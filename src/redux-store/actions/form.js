import Types from "./types";
const formInit = payload => {
  return {
    type: Types.form.FORM_INIT,
    payload
  };
};

var addFormData = payload => {
  return {
    type: Types.form.ADD_DATA,
    payload
  };
};

const formDestroy = payload => {
  return {
    type: Types.form.FORM_DESTORY,
    payload
  };
};

module.exports = {
  formInit,
  formDestroy,
  addFormData
};
