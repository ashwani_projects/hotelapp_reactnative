import { formInit, formDestroy, addFormData } from "./form";

import { addGuest,deleteGuest } from "./list";

module.exports = {
  formInit,
  formDestroy,
  addFormData,
  addGuest,
    deleteGuest
};
