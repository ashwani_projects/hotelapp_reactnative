const form = {
  FORM_DESTORY: "FORM_DESTORY",
  FORM_INIT: "FORM_INIT",
  ADD_DATA: "ADD_DATA"
};

const list = {
  ADD_TO_LIST: "ADD_TO_LIST",
  UPDATE_IN_LIST: "UPDATE_IN_LIST",
  DELETE_FROM_LIST: "DELETE_FROM_LIST"
};

module.exports = { form, list };
