import { createStore } from "redux";
import reducers from "./reducers/index";
var initialState = {};
import devToolsEnhancer from "remote-redux-devtools";
var store = createStore(reducers, initialState, devToolsEnhancer());
export default store;
