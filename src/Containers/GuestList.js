import React, { Component } from "react";
import { ListView, View, Text, TextInput, TouchableOpacity,Image } from "react-native";
import { connect, Provider } from "react-redux";
import { NativeRouter as Router, Route, Link } from "react-router-native";
import { deleteGuest } from "../redux-store/actions/index";
class List extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      dataSource: ds.cloneWithRows(this.props.list)
    };
    this.searchGuest = this.searchGuest.bind(this);
  }
  searchGuest(value) {
    let filteredList =
      this.props.list &&
      this.props.list.filter(row => {
        return row.data.fullName.match(value);
      });
    this.setState({ dataSource: this.state.dataSource.cloneWithRows(filteredList) });
  }
    componentWillReceiveProps(newProps){
        this.setState({ dataSource: this.state.dataSource.cloneWithRows(newProps.list)});
    }
  render() {
    let { history } = this.props;
    return (
      <View style={{flex:1}}>
        <TextInput style={{padding:10,height: 40, borderColor: 'gray', borderWidth: 1}} placeholder={"Search By Name"} onChangeText={value => this.searchGuest(value)} />
        <ListView
          dataSource={this.state.dataSource}
          renderRow={row => {
            return (
              <View style={{margin:10,flexDirection:"row",justifyContent:"space-between"}}><TouchableOpacity
                onPress={() => {
                  history.push({
                    pathname: `/addGuest`,
                    state: { formName: row.id, updateAction: true }
                  });
                }}
              >
                <Text>{`${row.data.fullName} - ${row.data.roomNumber || ""}`}</Text>
              </TouchableOpacity>
                  <TouchableOpacity onPress={()=>this.props.deleteGuest({name:row.id})}><Image
                      source={require('../public/images/trash-icon.png')}
                  /></TouchableOpacity>
              </View>
            );
          }}
        />
        <View style={{position:"absolute",bottom:20,right:10}}><Link to={`/addGuest`} underlayColor="#f0f4f7">
            <Image
                style={{marginBottom:10}}
                source={require('../public/images/ic_add.png')}
            />
        </Link>
          <Link to={`/Statics`} underlayColor="#f0f4f7">
              <Image
                  source={require('../public/images/documents.png')}
              />
          </Link></View>
      </View>
    );
  }
}
export default connect(({ list = [] }) => {
  return {
    list
  };
}, {deleteGuest})(List);
