import React, { Component } from "react";
import uuid from "uuid";
import PropTypes from "prop-types";
class DateInput extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.onChange = this.onChange.bind(this);
  }

  onChange(date) {
    let { form: { setValue } } = this.context;
    setValue(this.props.field, date);
  }

  render() {
    let { form: { getValue } } = this.context;
    return this.props.render({
      date: getValue(this.props.field),
      onDateChange: date => {
        this.onChange(date);
      }
    });
  }
}

DateInput.contextTypes = {
  form: PropTypes.object
};
export default DateInput;
