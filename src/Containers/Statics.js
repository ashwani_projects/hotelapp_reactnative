import React, { Component } from "react";
import {StyleSheet, View, Text, TouchableOpacity, StatusBar,TextInput,ScrollView,Image } from "react-native";
import {Pie} from 'react-native-pathjs-charts';
import { Link } from "react-router-native";
import { connect } from "react-redux";

const styles = StyleSheet.create({
    container: {
        flex: 10,
        alignItems: 'center'
    },
    title: {
        fontSize: 24,
        margin: 10
    }
});

class Statics extends Component{
    constructor(props){
        super(props);
    }
    render() {
        let maleArray = this.props.list.filter((value) => {
            return value.data.gender === "Male"
        });
        let femaleArrayLength = this.props.list.length - maleArray.length;
        let data = [{
            "name": "Male",
            "population": (maleArray.length || 0)+1
        }, {
            "name": "Female",
            "population": (femaleArrayLength || 0)+1
        }];

        let options = {
            margin: {
                top: 20,
                left: 20,
                right: 20,
                bottom: 20
            },
            width: 350,
            height: 350,
            color: '#2980B9',
            r: 50,
            R: 150,
            legendPosition: 'topLeft',
            animate: {
                type: 'oneByOne',
                duration: 200,
                fillTransition: 3
            },
            label: {
                fontFamily: 'Arial',
                fontSize: 8,
                fontWeight: true,
                color: '#ECF0F1'
            }
        };

        return (
            <View style={{flex:1}}>
                <View style={{backgroundColor:"#84F8E6",flex:1,flexDirection:"row",alignItems:"center"}}>
                    <Link to={`/`} underlayColor="#f0f4f7">
                        <Image
                            style={{marginHorizontal:20}}
                            source={require('../public/images/backIcon.png')}
                        />
                    </Link>
                    <Text>GENDER RATIO</Text></View>
                <View style={styles.container}>
                    <Pie
                        data={data}
                        options={options}
                        accessorKey="population" />
                </View>
            </View>
        );
    }
}

export default connect(({ list = [] }) => {
    return {
        list
    };
}, {})(Statics);