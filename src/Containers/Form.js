/*mine */

import React, { Component } from "react";
import uuid from "uuid";
import { Text, View, ListView, TextInput, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { addFormData, formDestroy, formInit } from "../redux-store/actions/form";
import { addGuest, updateGuest } from "../redux-store/actions/list";
import PropTypes from "prop-types";
import { Link } from "react-router-native";
class Form extends React.Component {
  constructor(props) {
    super(props);
    this.saveData = this.saveData.bind(this);
    this.updateData = this.updateData.bind(this);
    this.setValue = this.setValue.bind(this);
    this.getValue = this.getValue.bind(this);
  }
  setValue(field, value) {
    this.props.addFormData({ name: this.props.name, field, value });
  }
  getValue(field) {
    let { data } = this.props;
    return data[field];
  }
  getChildContext() {
    return {
      form: {
        setValue: this.setValue,
        getValue: this.getValue
      }
    };
  }

  saveData() {
    this.props.addGuest({ name: this.props.name, data: this.props.data });
  }

  updateData() {
    this.props.updateGuest({ name: this.props.name, data: this.props.data });
  }

  render() {
    let { children, location } = this.props;

    return (
      <View style={{flex:10}}>
        {children}
        <TouchableOpacity
          style={{ marginVertical: 10,alignSelf:"center",backgroundColor:"#D2E1DE",paddingHorizontal:20,paddingVertical:10 }}
          onPress={location.state && location.state.updateAction ? this.updateData : this.saveData}
        >
          <Text style={{color:"white"}}>{location.state && location.state.updateAction ? "UPDATE" : "SUBMIT"}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

Form.childContextTypes = {
  form: PropTypes.object
};

Form = connect(
  ({ form = {} }, ownProps) => {
    let formState = form[ownProps.name] || {};
    return {
      name: ownProps.name,
      data: formState.dataSet || {}
    };
  },
  {
    addFormData,
    formDestroy,
    formInit,
    addGuest,
    updateGuest
  }
)(Form);

module.exports = class F extends Component {
  constructor(props, context) {
    super(props, context);
    this.name =
      (this.props.location && this.props.location.state && this.props.location.state.formName) ||
      "form_wrapper" + uuid.v4();
  }

  render() {
    return <Form {...this.props} name={this.name} />;
  }
};
