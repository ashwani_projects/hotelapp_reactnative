import React, { Component } from "react";
import uuid from "uuid";
import PropTypes from "prop-types";
class FieldInput extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.onChange = this.onChange.bind(this);
  }

  onChange(value) {
    let { form: { setValue } } = this.context;
    setValue(this.props.field, value);
  }

  render() {
    let { form: { getValue } } = this.context;
    return this.props.render({
      value: getValue(this.props.field),
      onChangeText: value => {
        this.onChange(value);
      }
    });
  }
}

FieldInput.contextTypes = {
  form: PropTypes.object
};
export default FieldInput;
