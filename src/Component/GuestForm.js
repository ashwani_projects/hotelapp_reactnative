import React, { Component } from "react";
import { View, Text, TouchableOpacity, TextInput,Image } from "react-native";
import Form from "../Containers/Form";
import FieldInput from "../Containers/FieldInput";
import DateInput from "../Containers/DateInput";
import DropDown from "../Containers/DropDown";
import DatePicker from "react-native-datepicker";
import { Dropdown as ModalDropdown } from "react-native-material-dropdown";
import { Link } from "react-router-native";
class GuestForm extends React.Component {
  constructor(props, context) {
    super(props, context);
  }
  render() {
    return (
      <View style={{flex:1}}>
          <View style={{backgroundColor:"#84F8E6",flex:1,flexDirection:"row",alignItems:"center"}}>
              <Link to={`/`} underlayColor="#f0f4f7">
                  <Image
                      style={{marginHorizontal:20}}
                      source={require('../public/images/backIcon.png')}
                  />
              </Link>
              <Text>ADD YOUR DETAILS</Text></View>
      <Form {...this.props}>
        <View>
          <View style={{padding:10}}>
            <FieldInput
              field="fullName"
              render={props => {
                return <TextInput style={{padding:10,height: 40}} placeholder={"Full Name"} {...props} />;
              }}
            />
            <FieldInput
              field="roomNumber"
              render={props => {
                return <TextInput style={{padding:10,height: 40}} {...props} placeholder={"Room Number"} />;
              }}
            />
            <DateInput
              field="checkInDate"
              render={props => {
                return (
                  <DatePicker
                    style={{ width: 300,marginBottom:10 }}
                    mode="date"
                    placeholder="Select Check In Date"
                    format="YYYY-MM-DD"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: "absolute",
                        left: 0,
                        top: 4,
                        marginLeft: 0
                      },
                      dateInput: {
                        marginLeft: 36
                      }
                      // ... You can check the source to find the other keys.
                    }}
                    {...props}
                  />
                );
              }}
            />

            <DateInput
              field="checkOutDate"
              render={props => {
                return (
                  <DatePicker
                    style={{ width: 300 }}
                    mode="date"
                    placeholder="Select Check Out Date"
                    format="YYYY-MM-DD"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: "absolute",
                        left: 0,
                        top: 4,
                        marginLeft: 0
                      },
                      dateInput: {
                        marginLeft: 36
                      }
                      // ... You can check the source to find the other keys.
                    }}
                    {...props}
                  />
                );
              }}
            />

            <DropDown
              field="age"
              render={props => {
                return (
                  <ModalDropdown
                    label={"Select Age"}
                    {...props}
                    data={[{ value: "0-10" }, { value: "10-20" }, { value: "30+" }]}
                  />
                );
              }}
            />
            <DropDown
              field="gender"
              render={props => {
                return (
                  <ModalDropdown
                    label={"Select Gender"}
                    {...props}
                    data={[{ value: "Male" }, { value: "Female" }]}
                  />
                );
              }}
            />
            <DropDown
              field="travelType"
              render={props => {
                return (
                  <ModalDropdown
                    label={"Travel Type"}
                    {...props}
                    data={[
                      { value: "Business" },
                      { value: "Family" },
                      { value: "Solo" },
                      { value: "Leisure" },
                      { value: "Others" }
                    ]}
                  />
                );
              }}
            />
          </View>
        </View>
      </Form>
      </View>
    );
  }
}

export default GuestForm;
