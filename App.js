import React from 'react';
import {View,Text,StatusBar} from 'react-native';
import store from './src/redux-store/store'
import {connect, Provider} from 'react-redux';
 import GuestList from './src/Containers/GuestList';
 import GuestForm from './src/Component/GuestForm';
 import Statics from './src/Containers/Statics';
import { NativeRouter as Router, Route, Link } from 'react-router-native'


 class HotelApp extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (<View style={{marginTop:StatusBar.currentHeight,flexDirection:"column",flex:1}}>
            <Router>
                <View style={{flex:1}}>
                  <Route exact path={`/`} component={GuestList}/>
                  <Route path={`/addGuest`} component={GuestForm}/>
                  <Route path={`/statics`} component={Statics}/>
                </View>
            </Router>
        </View>)
    }
}
export default class Application extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <HotelApp />
            </Provider>
        );
    }
}
